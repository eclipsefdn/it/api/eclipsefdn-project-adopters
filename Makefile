SHELL = /bin/bash
install:;
	npm ci --no-cache

dev-start:;
	mvn compile -e quarkus:dev

clean:;
	mvn clean

compile-test-resources:;
	npm run clean
	npm run generate-json-schema
	
compile-assets: compile-test-resources;
	npm test && npm run build

compile-java:compile-assets;
	mvn compile package

compile-java-quick:compile-assets;
	mvn compile package -Dmaven.test.skip=true

compile: clean compile-java;

compile-quick: clean compile-java-quick;

compile-start: compile-quick;
	docker compose down
	docker compose build
	docker compose up -d