/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.adopters.test.helpers;

public class SchemaNamespaceHelper {
    public static final String BASE_SCHEMA_PATH = "schemas/";
    public static final String BASE_SCHEMA_PATH_SUFFIX = "-schema.json";

    public static final String ADOPTED_PROJECT_SCHEMA_PATH = BASE_SCHEMA_PATH + "adopted-project" + BASE_SCHEMA_PATH_SUFFIX;
    public static final String ADOPTED_PROJECTS_SCHEMA_PATH = BASE_SCHEMA_PATH + "adopted-projects" + BASE_SCHEMA_PATH_SUFFIX;
    public static final String ADOPTER_SCHEMA_PATH = BASE_SCHEMA_PATH + "adopter" + BASE_SCHEMA_PATH_SUFFIX;
    public static final String ADOPTERS = BASE_SCHEMA_PATH + "adopters" + BASE_SCHEMA_PATH_SUFFIX;
}
