/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.adopters.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.api.WorkingGroupsAPI;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.api.models.WorkingGroupBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupParticipationAgreementBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupParticipationAgreementsBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupParticipationLevelBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupResourcesBuilder;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@RestClient
@ApplicationScoped
public class MockWorkingGroupAPI implements WorkingGroupsAPI {

    private List<WorkingGroup> wgs;

    public MockWorkingGroupAPI() {
        this.wgs = new ArrayList<>();
        this.wgs
                .addAll(Arrays
                        .asList(WorkingGroupBuilder
                                .builder()
                                .alias("sample-wg")
                                .description("")
                                .levels(Arrays
                                        .asList(WorkingGroupWorkingGroupParticipationLevelBuilder
                                                .builder()
                                                .description("sample")
                                                .relation("WGSAMP")
                                                .build()))
                                .logo("")
                                .parentOrganization("eclipse")
                                .resources(WorkingGroupWorkingGroupResourcesBuilder
                                        .builder()
                                        .charter("")
                                        .contactForm("")
                                        .members("")
                                        .sponsorship("")
                                        .website("")
                                        .participationAgreements(WorkingGroupWorkingGroupParticipationAgreementsBuilder
                                                .builder()
                                                .individual(WorkingGroupWorkingGroupParticipationAgreementBuilder
                                                        .builder()
                                                        .documentId("sample-wg-iwgpa")
                                                        .pdf("sample-wg-iwgpa.pdf")
                                                        .build())
                                                .organization(WorkingGroupWorkingGroupParticipationAgreementBuilder
                                                        .builder()
                                                        .documentId("sample-wg-owgpa")
                                                        .pdf("sample-wg-owgpa.pdf")
                                                        .build())
                                                .build())
                                        .build())
                                .status("active")
                                .title("Sample WG")
                                .build()));
    }

    @Override
    public Uni<RestResponse<List<WorkingGroup>>> get(BaseAPIParameters baseParams, List<String> statuses) {
        return Uni
                .createFrom()
                .item(MockDataPaginationHandler
                        .paginateData(baseParams, wgs.stream().filter(wg -> statuses == null || statuses.contains(wg.status())).toList()));
    }
}
