/*********************************************************************
* Copyright (c) 2020, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.adopters.model;

import java.util.List;

import io.soabase.recordbuilder.core.RecordBuilder;

/**
 * A project with information about its adopters (read from the file system) included with the object.
 * 
 * @author Martin Lowe
 *
 */
@RecordBuilder
public record AdoptedProject(String projectId, String name, String url, String logo, List<Adopter> adopters) {

}
