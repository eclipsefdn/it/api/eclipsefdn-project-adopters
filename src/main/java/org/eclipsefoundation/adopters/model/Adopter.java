/*********************************************************************
* Copyright (c) 2020, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.adopters.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.soabase.recordbuilder.core.RecordBuilder;

/**
 * Represents an adopter from the serialized adopter.json file.
 * 
 * @author Martin Lowe
 *
 */
@RecordBuilder
public record Adopter(String name, @JsonProperty("homepage_url") String homepageUrl, String logo,
        @JsonProperty("logo_white") String logoWhite, List<String> projects) {

}
