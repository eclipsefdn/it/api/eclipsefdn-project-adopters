/*********************************************************************
* Copyright (c) 2020, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		Zachary Sabourin <zachary.sabourin@ecliupse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.adopters.resource;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.adopters.model.AdoptedProject;
import org.eclipsefoundation.adopters.model.Breadcrumb;
import org.eclipsefoundation.adopters.service.AdopterService;
import org.eclipsefoundation.caching.model.ParameterizedCacheKeyBuilder;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.caching.service.LoadingCacheManager;
import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.services.ProjectService;
import org.eclipsefoundation.efservices.services.WorkingGroupService;
import org.eclipsefoundation.http.namespace.CacheControlCommonValues;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.jboss.resteasy.reactive.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.qute.Location;
import io.quarkus.qute.Template;
import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Retrieves adopted projects along with adopters info for display. This data can be viewed for all projects, a single project, or all
 * projects defined within a working group by the working group ID.
 * 
 * Additionally contains the HTML UI pages for users to interact with this data.
 * 
 * @author Martin Lowe
 *
 */
@Path("")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class AdoptersResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdoptersResource.class);
    private static final String BREADCRUMB_TEMPLATE_PROPERTY = "breadcrumb";

    @Inject
    CachingService cache;
    @Inject
    LoadingCacheManager lcm;

    @Inject
    ProjectService projectService;
    @Inject
    AdopterService adopterService;
    @Inject
    WorkingGroupService wgs;

    @Location("pages/project-adopters-wg")
    Template adoptersWgTemplate;
    @Location("pages/how-to-be-listed")
    Template howToBeListedTemplate;
    @Location("pages/home")
    Template homeTemplate;
    @Location("pages/error")
    Template errorTemplate;

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public Response getHomePage() {
        // Sort all Wgs alphabetically
        List<WorkingGroup> workingGroups = wgs.get();
        workingGroups.sort(Comparator.comparing(WorkingGroup::title));
        return Response
                .ok(cache
                        .get("homepage-html", null, String.class,
                                () -> homeTemplate.data("workingGroups", workingGroups).data(BREADCRUMB_TEMPLATE_PROPERTY, null).render())
                        .data()
                        .orElseGet(errorTemplate::render))
                .build();
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("how-to-be-listed-as-an-adopter")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public Response getListedPage() {
        return Response
                .ok(cache
                        .get("how-to-be-listed", null, String.class,
                                () -> howToBeListedTemplate
                                        .data(BREADCRUMB_TEMPLATE_PROPERTY,
                                                new Breadcrumb("/adopters/how-to-be-listed-as-an-adopter",
                                                        "How to be listed as an Adopter"))
                                        .render())
                        .data()
                        .orElseGet(errorTemplate::render))
                .build();
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("project-adopters/{alias}")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public Response getWorkingGroupSubPage(@PathParam("alias") String workingGroupAlias) {
        // retrieve the corresponding working group, or return an error
        Optional<WorkingGroup> currentWg = wgs.getByName(workingGroupAlias);
        if (currentWg.isEmpty()) {
            // a bit redundant, but attempt to get a cached error page or render a fresh copy
            return Response
                    .ok(cache.get(workingGroupAlias, null, String.class, errorTemplate::render).data().orElseGet(errorTemplate::render))
                    .build();
        }
        // using the discovered working group, render the working group adopters page, w/ fallback on error page
        return Response
                .ok(cache
                        .get(workingGroupAlias, null, String.class, () -> adoptersWgTemplate
                                .data("wg", currentWg.get())
                                .data(BREADCRUMB_TEMPLATE_PROPERTY,
                                        new Breadcrumb("/adopters/project-adopters/" + workingGroupAlias, currentWg.get().title()))
                                .render())
                        .data()
                        .orElseGet(errorTemplate::render))
                .build();
    }

    @GET
    @Path("/projects")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public Response getAllAdopters(@QueryParam("working_group") String workingGroup, @QueryParam("interest_group") String interestGroup) {
        // get cached project list
        List<Project> projects = projectService.getAllProjects();
        if (StringUtils.isNotBlank(workingGroup)) {
            // check that the passed WG is a valid WG before filtering
            List<WorkingGroup> groups = lcm.getList(ParameterizedCacheKeyBuilder.builder().id("all").clazz(WorkingGroup.class).build());
            if (LOGGER.isDebugEnabled()) {
                LOGGER
                        .debug("Groups: {}, looking for {} ({})", groups, TransformationHelper.formatLog(workingGroup),
                                groups.stream().noneMatch(wg -> wg.alias().equals(workingGroup)));
            }
            if (groups.stream().noneMatch(wg -> wg.alias().equals(workingGroup))) {
                throw new BadRequestException("Passed working group is not a valid working group");
            }
            // filter the projects by the passed working group
            projects = projects
                    .stream()
                    .filter(p -> p.industryCollaborations().stream().anyMatch(wg -> wg.id().equals(workingGroup)))
                    .toList();
        }
        if (StringUtils.isNotBlank(interestGroup)) {
            // check that the passed IG is a valid IG before filtering
            List<InterestGroup> groups = lcm.getList(ParameterizedCacheKeyBuilder.builder().id("all").clazz(InterestGroup.class).build());
            if (LOGGER.isDebugEnabled()) {
                LOGGER
                        .debug("Groups: {}, looking for {} ({})", groups, TransformationHelper.formatLog(interestGroup),
                                groups.stream().noneMatch(ig -> ig.shortProjectId().equals(interestGroup)));
            }
            if (groups.stream().noneMatch(wg -> wg.shortProjectId().equals(interestGroup))) {
                throw new BadRequestException("Passed interest group is not a valid interest group");
            }
            // filter the projects by the passed working group
            projects = projects
                    .stream()
                    .filter(p -> p.industryCollaborations().stream().anyMatch(wg -> wg.id().equals(interestGroup)))
                    .toList();

        }
        // no projects for working group
        if (projects.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // get the adopted projects, removing non-adopted projects
        List<AdoptedProject> aps = adopterService.getAdoptedProjects(projects);
        if (aps == null) {
            return Response.serverError().build();
        }
        return Response.ok(aps).build();
    }

    @GET
    @Path("/projects/{projectId}")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public Response getAdoptersForProject(@PathParam("projectId") String projectId) {
        // get cached project list
        List<Project> projects = projectService.getAllProjects();
        List<Project> filteredProjects = projects.stream().filter(p -> p.projectId().equals(projectId)).toList();
        // no projects for working group
        if (filteredProjects.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // get the adopted projects, removing non-adopted projects
        List<AdoptedProject> aps = adopterService.getAdoptedProjects(filteredProjects);
        if (aps == null) {
            return Response.serverError().build();
        }
        return Response.ok(aps).build();
    }
}
