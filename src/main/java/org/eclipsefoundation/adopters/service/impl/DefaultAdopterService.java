/*********************************************************************
* Copyright (c) 2020, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*         Zachary Sabourin <zachary.sabourin@ecliupse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.adopters.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.adopters.model.AdoptedProject;
import org.eclipsefoundation.adopters.model.AdoptedProjectBuilder;
import org.eclipsefoundation.adopters.model.Adopter;
import org.eclipsefoundation.adopters.model.AdopterList;
import org.eclipsefoundation.adopters.service.AdopterService;
import org.eclipsefoundation.efservices.api.models.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.runtime.Startup;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 * Retrieves the adopters information from the filesystem on service start, and provides copies to requesting callers.
 * 
 * @author Martin Lowe, Zachary Sabourin
 *
 */
@Startup
@ApplicationScoped
public class DefaultAdopterService implements AdopterService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultAdopterService.class);

    @ConfigProperty(name = "eclipse.project-adopters.filepath", defaultValue = "adopters.json")
    String adoptersFilepath;

    @Inject
    ObjectMapper objectMapper;

    private List<Adopter> adopters;

    @PostConstruct
    public void init() throws IOException {
        // Attempt to load file on startup. Failing if there is an issue
        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(adoptersFilepath)) {
            AdopterList adopterList = objectMapper.readValue(is, AdopterList.class);
            this.adopters = new ArrayList<>(adopterList.adopters());
            LOGGER.info("Initialized {} adopters", adopters.size());
        }
    }

    @Override
    public List<Adopter> getAdopters() {
        // Return all adopters loaded from file
        return new ArrayList<>(adopters);
    }

    @Override
    public List<AdoptedProject> getAdoptedProjects(List<Project> projects) {
        // For each project, we create an AdoptedProject entity with all relevant adopters
        return projects.stream().map(this::getAdoptedProject).filter(a -> !a.adopters().isEmpty()).toList();
    }

    /**
     * Creates an AdoptedProject entity using the given project information. Adds all project adopters that match the given project's id.
     * 
     * @param p The given project
     * @return A constructed AdoptedProject entity with all relevant information.
     */
    private AdoptedProject getAdoptedProject(Project p) {
        return AdoptedProjectBuilder
                .builder()
                .projectId(p.projectId())
                .name(p.name())
                .logo(p.logo())
                .url(p.url())
                .adopters(getAdopters()
                        .stream()
                        .filter(a -> a.projects().contains(p.projectId()))
                        .sorted(Comparator.comparing(a -> a.name().toLowerCase()))
                        .toList())
                .build();
    }

}
